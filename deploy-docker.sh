#!/bin/bash
# Props
APP_NAME="backend-nestjs"
DOCKER_IMAGE="registry.gitlab.com/julien-ilari/backend-nestjs:master"

# Fonction pour afficher des messages de journalisation
log() {
  echo "$(date +'%Y-%m-%d %H:%M:%S') - $1"
}

# Fonction pour enregistrer un message d'erreur avec "Arrêt du déploiement"
log_error() {
  log "$1 Arrêt du déploiement."
  exit 1
}


# Fonction principale
main() {
    # Gestion des erreurs avec set -e
    set -e

    # Validation des variables d'environnement
    if [ -z "$SERVER_USER" ] || [ -z "$SERVER_IP" ] || [ -z "$SSH_PRIVATE_KEY" ]; then
        log_error "Erreur : Les variables d'environnement SERVER_USER, SERVER_IP et SSH_PRIVATE_KEY doivent être définies."
    fi

    # Configure le client SSH
    log "Installation du client SSH et démarrage de l'agent SSH"
    apk add --no-cache openssh-client && eval $(ssh-agent -s)
    ssh-add -D  # Efface toutes les clés de l'agent
    chmod 600 $SSH_PRIVATE_KEY
    ssh-add $SSH_PRIVATE_KEY

    log "Déploiement de l'application sur le serveur distant ..."

    # Utilisation d'une commande unique pour la connexion SSH
    ssh -o StrictHostKeyChecking=no -i $SSH_PRIVATE_KEY $SERVER_USER@$SERVER_IP <<EOF
        # Lancer les services docker
        docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        docker pull $DOCKER_IMAGE

        # Nettoyage global de Docker
        docker system prune -f > /dev/null 2>&1
EOF

    log "Déploiement de l'application réussi"
}

# Appel de la fonction principale en passant "--test" en paramètre pour activer le mode test si nécessaire
main "$@"