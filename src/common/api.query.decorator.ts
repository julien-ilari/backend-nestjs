import { applyDecorators, Type } from '@nestjs/common';
import { ApiOperation, ApiQuery } from '@nestjs/swagger';
import { ApiPaginatedResponse } from './pagination.schema';

export const ApiPaginationOperation = <
  TOperation extends string,
  TModel extends Type<any>,
  TAttributes extends Type<any>,
>(
  summary: TOperation,
  model: TModel,
  modelAtt: TAttributes, // modèle d'attributs
) => {
  return applyDecorators(
    ApiOperation({ summary }),
    ApiPaginatedResponse(model, modelAtt),
    ApiQuery({
      name: 'limit',
      type: 'number',
      example: '10',
      required: true,
      description: "Nombre d'éléments par page",
    }),
    ApiQuery({
      name: 'skip',
      type: 'number',
      example: '0',
      required: true,
      description: "Nombre d'éléments à sauter pour la pagination",
    }),
  );
};
