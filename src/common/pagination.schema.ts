import { applyDecorators, Type } from '@nestjs/common';
import { ApiExtraModels, ApiOkResponse, getSchemaPath } from '@nestjs/swagger';
import { DocumentHATEOS, ResourceData } from '../lib/app.responses.hal';
export const ApiPaginatedResponse = <
  TModel extends Type<ResourceData<any>>,
  TAttributes extends Type<any>,
>(
  model: TModel,
  modelAtt: TAttributes, // modèle d'attributs
) => {
  return applyDecorators(
    ApiExtraModels(DocumentHATEOS, model, modelAtt),
    ApiOkResponse({
      schema: {
        title: `PaginatedResponseOf${model.name}`,
        allOf: [
          { $ref: getSchemaPath(DocumentHATEOS) },
          {
            properties: {
              data: {
                type: 'array',
                items: { $ref: getSchemaPath(model) },
              },
            },
          },
        ],
        required: ['data'],
      },
    }),
    ApiOkResponse({
      schema: {
        type: 'object',
        properties: {
          links: { type: 'object' },
          data: {
            type: 'array',
            items: {
              type: 'object',
              properties: {
                id: { type: 'string' },
                attributes: { $ref: getSchemaPath(modelAtt) }, // Utilisez getSchemaPath avec attributesModel
              },
            },
          },
        },
      },
    }),
  );
};
