import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Response } from 'express';
import {
  CollectionHATEOS,
  DocumentHATEOS,
  PageProps,
} from '../lib/app.responses.hal';
import { instanceToInstance, plainToClass } from 'class-transformer';

@Injectable()
export class TransformInterceptor<T>
  implements NestInterceptor<T, DocumentHATEOS<T> | CollectionHATEOS<T>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<DocumentHATEOS<T> | CollectionHATEOS<T>> {
    const response: Response = context.switchToHttp().getResponse();
    const request = context.switchToHttp().getRequest();

    return next.handle().pipe(
      map((data) => {
        const baseUrl = request.protocol + '://' + request.get('host');
        const selfUrl = baseUrl + request.originalUrl;

        if (Array.isArray(data)) {
          const document = new CollectionHATEOS<T>(data);

          document.pagination = {
            count: data.length,
            offset: 0,
            limit: 10,
            total: data.length,
          };

          // Ajout de la pagination dans le header
          // Nombre maxinum d'élément.
          response.setHeader(
            'Pagination-Limit',
            document.pagination.total.toString(),
          );
          // Nombre d'éléments ignorés .
          response.setHeader(
            'Pagination-Offset',
            document.pagination.total.toString(),
          );
          // Nombre réel de résultats renvoyés.
          response.setHeader(
            'Pagination-Count',
            document.pagination.total.toString(),
          );

          // Nombre total de résultats correspondant à la requête.
          response.setHeader(
            'Pagination-Total',
            document.pagination.total.toString(),
          );

          document.addLink('self', selfUrl);
          return document;
        } else {
          const document = new DocumentHATEOS<T>(data);
          document.addLink('self', selfUrl);

          return document;
        }
      }),
    );
  }
}
