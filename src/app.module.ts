import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CategoriesModule } from './modules/catalogue/categories/categories.module';
import { ProductsModule } from './modules/catalogue/products/products.module';
import { OrdersModule } from './modules/orders/orders.module';
import { DeliveriesModule } from './modules/deliveries/deliveries.module';
import { InvoicesModule } from './modules/orders/invoices/invoices.module';
import { ShoppingCartsModule } from './modules/shopping-carts/shopping-carts.module';
import { ProductTranslationsModule } from './modules/catalogue/products/product-translations/product-translations.module';
import { WorkflowModule } from './modules/workflow/workflow.module';

@Module({
  imports: [
    MongooseModule.forRoot(
      process.env.MONGO_URI || 'mongodb://localhost:27017/ecommerceDB',
    ),
    ProductsModule,
    ProductTranslationsModule,
    CategoriesModule,
    OrdersModule,
    DeliveriesModule,
    InvoicesModule,
    ShoppingCartsModule,
    WorkflowModule,
  ],
})
export class AppModule {}
