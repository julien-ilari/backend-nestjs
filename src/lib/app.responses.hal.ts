import { ApiProperty } from '@nestjs/swagger';
import { ElementType, EntityProps } from './common';
import {
  Expose,
  classToClassFromExist,
  classToPlain,
  classToPlainFromExist,
  instanceToInstance,
  instanceToPlain,
  plainToClass,
  plainToClassFromExist,
  plainToInstance,
  serialize,
} from 'class-transformer';
import { ProductAttributes } from 'src/modules/catalogue/products/data/product.data';

/**
 * La classe DocumentHATEOS<T> est utilisée pour représenter une ressource unique, elle contient
 * un objet _data qui représente les données de la ressource, ainsi qu'un objet _links qui contient des liens vers des ressources connexes.
 */
export class DocumentHATEOS<T> {
  @ApiProperty({
    readOnly: true,
    description: 'Un objet qui contient des liens vers des ressources connexes',
  })
  links: Record<string, string>;

  @ApiProperty({
    readOnly: true,
    description: 'Un objet qui contient les données de la ressource.',
  })
  data: T;

  constructor(data: T) {
    this.data = data;
  }

  addLink(rel: string, href: string) {
    if (!this.links) {
      this.links = {};
    }
    this.links[rel] = href;

    return this;
  }

  static create<T>(data: T): DocumentHATEOS<T> {
    return new DocumentHATEOS<T>(data);
  }
}

/**
 * La classe CollectionHATEOS<T> quant à elle est utilisée pour représenter une collection de ressources.
 * Elle contient des propriétés pour la pagination (page, limit, total) ainsi que des objets _links et _data pour les liens et les données de la collection.
 */
export class CollectionHATEOS<T> {
  @ApiProperty({
    readOnly: true,
    description: 'Un objet qui contient les données de la collection.',
  })
  data: ResourceData<T> | ResourceData<T>[];

  pagination?: PageProps;

  @ApiProperty({
    readOnly: true,
    description: 'Un objet qui contient des liens vers des ressources connexes',
  })
  links: Record<string, string>;

  included: Record<string, any>;

  constructor(data: ResourceData<T>[], links?: Record<string, string>) {
    this.data = this.extractData(data);
    this.links = links || {};
  }

  private extractData(
    data: ResourceData<T>[],
  ): ResourceData<T> | ResourceData<T>[] {
    if (Array.isArray(data)) {
      return data.map((item) => this.extractResourceData(item));
    } else {
      return this.extractResourceData(data);
    }
  }

  /**
   * Extrait les données pertinentes d'un élément de ressource.
   * @param item - L'élément de ressource à partir duquel extraire les données.
   * @returns Les données formatées en ResourceData.
   */
  private extractResourceData(
    resourceData: ResourceData<T>,
  ): ResourceData<T> | any {
    const { relationships } = resourceData;

    // Traitement des relations si elles sont présentes
    if (relationships) {
      for (const [key, values] of Object.entries(relationships)) {
        // Vérifier si values est un tableau
        if (Array.isArray(values)) {
          /**
           * Traitement des Relations : Le code vérifie d'abord si des relations sont présentes.
           * Ensuite, il itère sur chaque relation pour extraire les IDs et les stocker dans resourceData.relationships.
           */
          // Mettre à jour resourceData.relationships avec les relations accumulées et nouvelles
          resourceData.relationships = {
            ...resourceData.relationships,
            [key]: values.map((relationship: any) => relationship.id),
          };
          /**
           * Mise à Jour de included : Pour chaque relation, le code met à jour included en évitant la duplication.
           * Il utilise un ensemble (Set) pour s'assurer qu'il n'y a pas de doublons lors de l'ajout des nouvelles valeurs.
           */
          // Mettre à jour included avec les relations accumulées et nouvelles
          this.included = {
            ...this.included,
            [key]: Array.from(
              new Set([
                ...(this.included?.[key] ?? []),
                ...(relationships?.[key] ?? []),
              ]),
            ),
          };
        }
      }
    }

    // Retourne le ResourceData final
    return resourceData;
  }

  addLink(rel: string, href: string): CollectionHATEOS<T> {
    if (!this.links) {
      this.links = {};
    }
    this.links[rel] = href;
    return this;
  }

  static create<T>(
    data: ResourceData<ResourceData<T>>[],
    links?: Record<string, string>,
  ): CollectionHATEOS<ResourceData<T>> {
    return new CollectionHATEOS<ResourceData<T>>(data, links);
  }
} // fin CollectionHATEOS<T>

export type Meta = PageProps & {
  debug?: {
    prepareParams: number;
    callQuery: number;
    transform: number;
  };
};

export class PageProps {
  @ApiProperty({
    type: Number,
    readOnly: true,
    description: 'Le nombre de item retournés par la page',
  })
  count: number;

  @ApiProperty({
    type: Number,
    readOnly: true,
    description: 'Le numéro de la page actuelle',
  })
  offset: number;

  @ApiProperty({
    type: Number,
    readOnly: true,
    description: "Nombre maxinum d'élément dans la page",
  })
  limit: number;

  @ApiProperty({
    type: Number,
    readOnly: true,
    description: ' Le nombre total items dans la collection',
  })
  total: number;
}

export type ResourceData<T> = {
  id: string;
  type: string;

  attributes?: Attributes<Omit<T, 'id'>>;

  embedded?: any;
  relationships?: any;

  links?: Links;
};

export type Attributes<D> = {
  [P in EntityProps<D>]: ElementType<D[P]>;
};

export type Links = {
  self: string;
  related?: string;
};

export interface Relationships {
  [key: string]: Relationship[];
}

export interface Relationship {
  id: string;
  type: string;
  link: string;
}
