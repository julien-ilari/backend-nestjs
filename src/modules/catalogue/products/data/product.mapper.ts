import {
  Product,
  ProductDocument,
  ProductLangDocument,
} from './product.schema';
import {
  ProductReadData,
  ProductSaveData,
  ProductMergeData,
  ProductAttributes,
  ProductSEOAttributes,
  ProductTranslationAttributes,
} from './product.data';
import { plainToInstance } from 'class-transformer';

export class ProductWithOptions {
  withTranslations?: boolean;
  withRelationships?: boolean;
}

export class ProductMapper {
  static toSchema(data: ProductSaveData | ProductMergeData): Product {
    const document = new Product();
    document.reference = data.reference;
    document.price = data.price;

    return document;
  }

  static toProductLanguage(productLangDocument: ProductLangDocument): any {
    return {
      translation: plainToInstance(
        ProductTranslationAttributes,
        productLangDocument,
        {
          excludeExtraneousValues: true,
        },
      ),
      seo: plainToInstance(ProductSEOAttributes, productLangDocument, {
        excludeExtraneousValues: true,
      }),
    };
  }

  static toProductLanguages(translations: ProductLangDocument[]): any {
    const translationsByLanguage = {};
    for (const productLangDocument of translations) {
      if (!translationsByLanguage[productLangDocument.languageCode]) {
        translationsByLanguage[productLangDocument.languageCode] = [];
      }
      translationsByLanguage[productLangDocument.languageCode] =
        this.toProductLanguage(productLangDocument);
    }

    // Copier les propriétés de la traduction dans une nouvelle instance
    return translationsByLanguage;
  }

  // translation.languageCode

  static toData(schema: ProductDocument): ProductReadData {
    // Copie les propriétés dans la nouvelle instance
    const data = new ProductReadData(schema.id);

    // Utilisez plainToClass pour convertir votre objet schema en une instance de ProductAttributes
    data.attributes = plainToInstance(ProductAttributes, schema, {
      excludeExtraneousValues: true, // exclut les propriétés non définies dans ProductAttributes
    });

    data.additionalProperties = {};

    // Personnalisation de la réponse en fonction des besoins de l'application.
    // Embedded (Intégré) : Utilisé pour stocker des données directement liées au produit lui-même, telles que les images ou les traductions.
    // Ces données sont des parties intégrantes du produit.
    data.embedded = {
      'product-images': schema.images,
      // Créer un objet pour stocker les traductions par language
      'product-langs': this.toProductLanguages(schema['product-translations']),
      'product-relationships': [],
      'product-variations': [],
    };

    /**
     * Links construction
     */
    data._links = { self: '/products/' + schema.id };

    /**
     * Relationships (Relations) : Utilisé pour stocker des liens vers d'autres entités,
     * comme les catégories, qui peuvent être partagées avec d'autres produits mais ne font pas partie intégrante des attributs du produit lui-même.
     * Cela permet de gérer les connexions entre les différents éléments de manière structurée.
     */
    data.relationships = {
      categories: [
        {
          id: '1',
          type: 'category',
          attributes: { describe: 'catégorie de test', isVisible: false },
        },
        {
          id: '2',
          type: 'category',
          attributes: { describe: 'catégorie de test', isVisible: false },
        },
        {
          id: '3',
          type: 'category',
          attributes: { describe: 'catégorie de test', isVisible: false },
        },
      ],
    };

    return data;
  }
}
