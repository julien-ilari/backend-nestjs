import { ApiProperty, ApiPropertyOptional, PartialType } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import { ResourceData } from 'src/lib/app.responses.hal';
import { Expose } from 'class-transformer';

interface HasId {
  id: string;
}

export class ProductTranslationAttributes {
  @Expose()
  name: string;

  @Expose()
  description: string;
}

export class ProductSEOAttributes {
  @Expose()
  meta_title: string;

  @Expose()
  meta_description: string;

  @Expose()
  meta_keywords: string;
}

export class ProductAttributes {
  // Getter pour la propriété 'type'

  @ApiProperty({
    type: Number,
    description: 'Prix de base sans aucune variation',
  })
  @IsNumber()
  @Expose()
  price: number;

  @ApiProperty({
    type: String,
    description: 'Référence unique du produit',
  })
  @IsString()
  reference = 'undefined';

  @ApiProperty({
    type: String,
    description: 'Nom du produit du produit',
  })
  @IsString()
  @Expose()
  name = 'undefined';

  @ApiProperty({
    type: String,
    description: 'Description du produit',
  })
  @IsString()
  description = 'undefined';
}

/**
 * Bean de présentation pour la lecture du produit
 */
export class ProductReadData implements ResourceData<ProductAttributes>, HasId {
  @ApiProperty({
    name: 'id',
    readOnly: true,
    type: 'String',
    description: 'Nom du produit',
  })
  id: string;

  type = 'product';

  @ApiProperty({
    name: 'attributes',
    type: 'ProductAttributes',
    description: 'Attribut du produit',
  })
  attributes: ProductAttributes;

  embedded: any;

  relationships: any;

  additionalProperties: any;
  _links?: { self: any };

  constructor(id?: string) {
    if (id !== undefined) {
      this.id = id;
    }
  }
}

/**
 * Bean de présentation pour l'enregistrement
 */
export class ProductSaveData extends ProductAttributes {
  type: string;
}

/**
 * Bean de présentation pour l'enregistrement (partiel)
 */
export class ProductMergeData extends PartialType(ProductAttributes) {
  @ApiPropertyOptional()
  @IsOptional()
  name: string;

  @ApiProperty({
    type: String,
    description: 'Type du produit',
  })
  @IsString()
  type: string;

  @ApiPropertyOptional()
  @IsOptional()
  description: string;

  @ApiPropertyOptional()
  @IsOptional()
  price: number;

  @ApiPropertyOptional()
  @IsOptional()
  image: string;

  @ApiPropertyOptional()
  @IsOptional()
  reference: string;
}
