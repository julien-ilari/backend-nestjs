// Voir https://docs.commercetools.com/api/projects/products#productdata
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

/**
 * La disponibilité du produit (par exemple, en stock, en rupture de stock).
 */
export enum ProductAvailability {
  InStock = 'in_stock',
  OutOfStock = 'out_of_stock',
  PreOrder = 'pre_order',
  Discontinued = 'discontinued',
}

/**
 * La note moyenne du produit.
 */
export enum ProductRating {
  OneStar = 1,
  TwoStars = 2,
  ThreeStars = 3,
  FourStars = 4,
  FiveStars = 5,
}

export type ProductDocument = Product & Document;
@Schema({ timestamps: true, versionKey: false })
export class Product {
  @Prop()
  reference: string;

  @Prop({ type: Boolean, default: false })
  isPublished: boolean;

  @Prop({ type: String, default: false })
  storeHash = '8743b52063cd84097a65d1633f5c74f5';

  @Prop()
  price: number;

  @Prop()
  name: string;

  // Actifs multimédias de la variante de produit.
  @Prop()
  assets: [];

  @Prop()
  images: [
    {
      order: 1;

      // description dans la langue de la boutique
      description: '';

      // L'URL de l'image original téléchargé
      url_original: 'https://fakeimg.pl/350x200/?text=FAKE';

      // L'URL de la miniature (page produits de la catégorie).
      url_thumbnail: 'https://fakeimg.pl/350x200/?text=FAKE';

      // L'URL de la petite image (vignette sous l'image du produit).
      url_small_thumbnail: 'https://fakeimg.pl/450x300/?text=FAKE';

      // L'URL standard de cette image (page produit).
      url_standard: 'https://fakeimg.pl/1250x1.1024/?text=FAKE';

      // L'URL de la petite image (image zoom produit).
      url_big: 'https://fakeimg.pl/1200x1700/?text=FAKE';
    },
  ];

  @Prop()
  availability: ProductAvailability;

  @Prop()
  rating: ProductRating;

  @Prop({
    type: [{ type: Types.ObjectId, ref: 'ProductLang' }],
  })
  'product-translations': ProductLangDocument[];

  @Prop()
  'product-variations': [
    {
      type: Types.ObjectId;
      ref: 'Product';
    },
  ];

  @Prop()
  'product-relationships': [
    {
      type: Types.ObjectId;
      ref: 'Product';
    },
  ];
}
export const ProductSchema = SchemaFactory.createForClass(Product);

export type ProductLangDocument = ProductLang & Document;
@Schema()
export class ProductLang {
  [x: string]: any;

  @Prop({ type: Types.ObjectId, ref: 'Product' })
  product: Product;

  @Prop()
  languageCode: string;

  @Prop()
  key: string;

  @Prop()
  name: string;

  @Prop()
  description: string;

  @Prop()
  meta_title: string;

  @Prop()
  meta_description: string;

  @Prop()
  meta_keywords: string;
}

export const ProductLangSchema = SchemaFactory.createForClass(ProductLang);
// ProductLangSchema.index({ product: 1, languageCode: 1 }, { unique: true });

export type TagTranslationDocument = TagTranslation & Document;

@Schema()
export class TagTranslation {
  @Prop()
  languageCode: string;

  @Prop()
  name: string;

  @Prop()
  description: string;
}

export const TagTranslationSchema =
  SchemaFactory.createForClass(TagTranslation);
