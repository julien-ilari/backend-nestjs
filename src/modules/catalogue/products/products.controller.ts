import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Patch,
  Post,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  getSchemaPath,
} from '@nestjs/swagger';
import { TransformInterceptor } from '../../../common/interceptor.controller';
import {
  ProductAttributes,
  ProductMergeData,
  ProductReadData,
  ProductSaveData,
} from './data/product.data';
import { ProductsService } from './products.service';
import { ApiPaginationOperation } from '../../../common/api.query.decorator';

@ApiTags('[Catalogue] Gestion du catalogue des produits')
@Controller('products')
@ApiExtraModels(ProductReadData, ProductSaveData, ProductMergeData)
@UseInterceptors(TransformInterceptor)
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  schemaProduct = {
    properties: {
      _data: {
        $ref: getSchemaPath(ProductReadData),
      },
    },
  };

  @ApiOperation({ summary: 'Récupérer les produits' })
  @ApiOkResponse({
    isArray: false, // Indique que la réponse n'est pas un tableau directement
    description: 'Description de la réponse',
    schema: {
      properties: {
        data: {
          type: 'array',
          items: {
            $ref: getSchemaPath(ProductReadData), // Utilisez getSchemaPath pour référencer le schéma de ProductReadData
          },
        },
      },
      required: ['page', 'limit', 'total', 'links', 'data'],
    },
  })
  @Get()
  async findAll(): Promise<ProductReadData[]> {
    return await this.productsService.findAll();
  }

  @ApiPaginationOperation(
    'Récupérer les produits (réponse paginée)',
    ProductReadData,
    ProductAttributes,
  ) // Utilisez votre annotation personnalisée ici
  @Get('/paginate')
  async findAllPaginate(
    @Query('limit') limit = 10,
    @Query('skip') skip = 0,
  ): Promise<ProductReadData[]> {
    return await this.productsService.paginate(limit, skip);
  }

  @ApiOperation({ summary: 'Créer un nouveau produit' })
  @ApiOkResponse({
    schema: {
      properties: {
        _data: {
          $ref: getSchemaPath(ProductReadData),
        },
      },
    },
  })
  @Post()
  @HttpCode(201) // retourne le code HTTP 201 au lieu de 200 par défaut
  async create(@Body() product: ProductSaveData): Promise<ProductReadData> {
    return await this.productsService.create(product);
  }

  @ApiOperation({ summary: 'Récupérer un produit de la liste' })
  @ApiOkResponse({
    schema: {
      properties: {
        _data: {
          $ref: getSchemaPath(ProductReadData),
        },
      },
    },
  })
  @Get(':id')
  @HttpCode(200)
  async findOne(@Param('id') id: string): Promise<ProductReadData> {
    return await this.productsService.findOne(id);
  }

  @ApiOperation({ summary: 'Mise à jour partielle du produit' })
  @ApiOkResponse({
    schema: {
      properties: {
        _data: {
          $ref: getSchemaPath(ProductReadData),
        },
      },
    },
  })
  @Patch(':id')
  @HttpCode(200)
  async patch(
    @Param('id') id: string,
    @Body() product: ProductMergeData,
  ): Promise<ProductReadData> {
    return await this.productsService.merge(id, product);
  }

  @Delete(':id')
  @HttpCode(200)
  async delete(@Param('id') _id: string): Promise<any> {
    return {};
  }
}
