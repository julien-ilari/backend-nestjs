import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ProductLang, ProductLangDocument } from '../data/product.schema';
import { ProductMapper } from '../data/product.mapper';

@Injectable()
export class ProductTranslationsService {
  constructor(
    @InjectModel(ProductLang.name)
    private readonly productLangModel: Model<ProductLangDocument>,
  ) {}

  async findLangs(): Promise<ProductLang[]> {
    const document = await this.productLangModel.find().exec();

    return ProductMapper.toProductLanguages(document);
  }

  async findOne(id: string): Promise<ProductLang> {
    const document = await this.productLangModel.findById(id).exec();

    if (!document) {
      throw new NotFoundException({
        objectOrError: `Product ${id} not found ${document}`,
        descriptionOrOptions: 'The product not exist or befor delete ',
      });
    }

    return ProductMapper.toProductLanguage(document);
  }
}
