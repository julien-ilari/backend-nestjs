import {
  Controller,
  Get,
  HttpCode,
  Param,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { TransformInterceptor } from '../../../../common/interceptor.controller';
import { ProductTranslationsService } from './product-translations.service';
import { ProductLang } from '../data/product.schema';

@ApiTags('[International] Gestion des traductions de produits')
@Controller('product-translations')
@UseInterceptors(TransformInterceptor)
export class ProductTranslationsController {
  constructor(private readonly productsService: ProductTranslationsService) {}

  @ApiOperation({ summary: 'Récupérer les traductions de produits' })
  @Get()
  async findAll(): Promise<ProductLang[]> {
    return this.productsService.findLangs();
  }

  @Get(':id')
  @HttpCode(200)
  @ApiOperation({ summary: 'Récupérer un produit de la liste' })
  async findOne(@Param('id') id: string): Promise<ProductLang> {
    return await this.productsService.findOne(id);
  }

  @ApiOperation({ summary: 'Céer une nouvelle traduction produit' })
  @Post()
  async create(): Promise<any> {
    return {};
  }
}
