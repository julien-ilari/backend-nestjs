import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  Product,
  ProductLang,
  ProductLangSchema,
  ProductSchema,
} from '../data/product.schema';
import { ProductTranslationsController } from './product-translations.controller';
import { ProductTranslationsService } from './product-translations.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Product.name, schema: ProductSchema },
      { name: ProductLang.name, schema: ProductLangSchema },
    ]),
  ],
  controllers: [ProductTranslationsController],
  providers: [ProductTranslationsService],
})
export class ProductTranslationsModule {}
