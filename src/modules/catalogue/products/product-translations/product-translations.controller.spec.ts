import { Test, TestingModule } from '@nestjs/testing';
import { ProductTranslationsController } from './product-translations.controller';
import { ProductTranslationsService } from './product-translations.service';
import { ProductTranslationsModule } from './product-translations.module';

describe('ProductTranslationsController', () => {
  let controller: ProductTranslationsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ProductTranslationsModule],
      controllers: [ProductTranslationsController],
      providers: [ProductTranslationsService],
    }).compile();

    controller = module.get<ProductTranslationsController>(
      ProductTranslationsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
