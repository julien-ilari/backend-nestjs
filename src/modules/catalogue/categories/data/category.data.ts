import { ApiProperty, ApiPropertyOptional, PartialType } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import { ResourceData } from 'src/lib/app.responses.hal';
import { Expose } from 'class-transformer';

interface HasId {
  id: string;
}

export class CategoryAttributes {
  @ApiProperty({
    type: String,
    description: 'Description complète de la catégorie',
  })
  @IsString()
  @Expose()
  description: string;

  @ApiProperty({
    type: String,
    description: 'Description courte',
  })
  @IsString()
  @Expose()
  summary: string;
}

/**
 * Bean de présentation pour la lecture de la catégory
 */
export class CategoryReadData
  implements ResourceData<CategoryAttributes>, HasId
{
  @ApiProperty({
    name: 'id',
    readOnly: true,
    type: 'String',
    description: 'Identifiant de la catégorie',
  })
  id: string;

  type = 'product';

  @ApiProperty({
    name: 'attributes',
    type: 'ProductAttributes',
    description: 'Attribut du produit',
  })
  attributes: CategoryAttributes;

  embedded?: any;

  relationships?: any;
}

/**
 * Bean de présentation pour l'enregistrement
 */
export class CategorySaveData extends CategoryAttributes {
  type: string;
}

/**
 * Bean de présentation pour l'enregistrement (partiel)
 */
export class CategoryMergeData extends PartialType(CategoryAttributes) {
  @ApiPropertyOptional()
  @IsOptional()
  name: string;

  @ApiProperty({
    type: String,
    description: 'Type du produit',
  })
  @IsString()
  type: string;

  @ApiPropertyOptional()
  @IsOptional()
  description: string;

  @ApiPropertyOptional()
  @IsOptional()
  price: number;

  @ApiPropertyOptional()
  @IsOptional()
  image: string;

  @ApiPropertyOptional()
  @IsOptional()
  reference: string;
}
