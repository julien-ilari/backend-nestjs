import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type CategoryDocument = Category & Document;
@Schema({ timestamps: true, versionKey: false })
export class Category {
  @Prop()
  description: string;

  @Prop()
  summary: string;
}
export const CategorySchema = SchemaFactory.createForClass(Category);
