import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Category, CategoryDocument } from './data/category.schema';
import {
  CategoryMergeData,
  CategoryReadData,
  CategorySaveData,
} from './data/category.data';

function toData(
  doc: CategoryDocument & { _id: Types.ObjectId },
  index?: number,
) {
  return {
    id: doc._id,
    type: 'category',
    attributes: {
      description: doc.description,
      summary: doc.summary,
    },
    metadata: {
      html: {
        color: index % 2 === 0 ? '#FD6C9E' : '#0000FF',
      },
    },
    embedded: undefined,
    relationships: undefined,
  };
}

@Injectable()
export class CategoriesService {
  constructor(
    @InjectModel(Category.name)
    private readonly model: Model<CategoryDocument>,
  ) {}

  async findAll(): Promise<CategoryReadData[]> {
    const documents = await this.model.find().exec();
    console.debug(documents);

    return documents.map((doc, index) => toData(doc, index));
  }

  async create(data: CategorySaveData): Promise<CategoryReadData> {
    const schema = this.toSchema(data);
    const saved = await new this.model(schema).save();

    return toData(saved);
  }

  toSchema(data: CategorySaveData | CategoryMergeData): Category {
    const document = new Category();
    document.description = data.description;
    document.summary = data.summary;

    return document;
  }
}
