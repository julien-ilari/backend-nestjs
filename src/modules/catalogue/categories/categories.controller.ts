import { Body, Controller, Get, Post, UseInterceptors } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { TransformInterceptor } from 'src/common/interceptor.controller';
import { CategoryReadData, CategorySaveData } from './data/category.data';
import { CategoriesService } from './categories.service';

@UseInterceptors(TransformInterceptor)
@ApiTags('[Catalogue] Gestion des catégoies de produits et sous-catégories')
@Controller('categories')
export class CategoriesController {
  constructor(private readonly service: CategoriesService) {}

  @ApiOperation({ summary: 'Récupérer toutes les catégories' })
  @Get()
  async findAll(): Promise<any> {
    return this.service.findAll();
  }

  @ApiOperation({ summary: 'Céer une nouvelle catégorie' })
  @Post()
  async create(@Body() body: CategorySaveData): Promise<CategoryReadData> {
    return this.service.create(body);
  }
}
