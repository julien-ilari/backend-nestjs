import { Module } from '@nestjs/common';
import { WorkflowController } from './workflow.controller';
import { WorkflowService } from './workflow.service';
import {
  CustomerDemand,
  SupportManagement,
  CustomerDemandSchema,
  SupportManagementSchema,
} from './workflow.schema';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: CustomerDemand.name, schema: CustomerDemandSchema },
      { name: SupportManagement.name, schema: SupportManagementSchema },
    ]),
  ],
  controllers: [WorkflowController],
  providers: [WorkflowService],
})
export class WorkflowModule {}
