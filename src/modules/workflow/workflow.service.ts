import { Injectable } from '@nestjs/common';
import {
  CustomerDemand,
  CustomerDemandDocument,
  SupportManagement,
  SupportManagementDocument,
} from './workflow.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { FilterDto } from './dto/workflow.dto';

@Injectable()
export class WorkflowService {
  constructor(
    @InjectModel(CustomerDemand.name)
    private readonly customerDemand: Model<CustomerDemandDocument>,

    @InjectModel(SupportManagement.name)
    private readonly supportManagement: Model<SupportManagementDocument>,
  ) {}

  async findAll(
    filter: FilterDto,
    skip?: number,
    limit?: number,
  ): Promise<any[]> {
    const documents = await this.customerDemand
      .find(filter.demandType ? { demandType: filter.demandType } : {})
      .skip(skip)
      .limit(limit)
      .exec();
    return documents;
  }

  async init(batchSize: number, totalDocuments: number) {
    const batch = batchSize; // Nombre de documents à insérer à la fois
    const documents = totalDocuments; // Total des documents à insérer
    const batchesCount = Math.ceil(documents / batch);
    const startTime = Date.now(); // Enregistrer le temps de début
    for (let i = 0; i < batchesCount; i++) {
      const batchCustomerDemand: CustomerDemand[] = [];
      const batchSupportManagement: SupportManagement[] = [];

      const start = i * batch;
      const end = Math.min(start + batch, documents);

      for (let j = start; j < end; j++) {
        const support = SupportManagement.generateRandom();

        batchSupportManagement.push(support);
        batchCustomerDemand.push(CustomerDemand.generateRandom(support));
      }

      await this.supportManagement.insertMany(batchSupportManagement);
      await this.customerDemand.insertMany(batchCustomerDemand);
      const executionTime = Date.now() - startTime; // Calculer la durée d'exécution en millisecondes
      console.log(
        `Temps total d'exécution insert (+ ${batchCustomerDemand.length} documents) :`,
        executionTime / 1000,
        'secondes',
      );
    }
  }

  async deleteAllData(): Promise<{ message: string }> {
    await this.customerDemand.deleteMany({});
    await this.supportManagement.deleteMany({});
    return { message: 'Toutes les données ont été supprimées avec succès.' };
  }
}
