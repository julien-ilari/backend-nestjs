import { ApiProperty } from '@nestjs/swagger';
import { IsInt, Min } from 'class-validator';

export class InitializationDto {
  @ApiProperty({
    example: 100,
    description: 'Nombre de documents à insérer à la fois',
  })
  @IsInt()
  @Min(1)
  batchSize: number;

  @ApiProperty({ example: 1, description: 'Total des documents à insérer' })
  @IsInt()
  @Min(1)
  totalDocuments: number;
}

export class FilterDto {
  @ApiProperty({
    required: false,
    example: 'snake',
    description: 'Type de la demande',
  })
  demandType?: string;

  @ApiProperty({
    example: 1,
    description: 'Page',
  })
  page: number;

  @ApiProperty({
    example: 100,
    description: 'Limite',
  })
  limit: number;
}
