import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { faker } from '@faker-js/faker';

/**
 * La note moyenne du produit.
 */
export enum DemandStatus {
  Cree = 1,
  EnCours = 2,
  Supprimer = 3,
}

export type StepDocument = Step & Document;
@Schema()
export class Step {
  @Prop()
  _id: Types.ObjectId; // Déclaration de _id

  @Prop()
  previousStep: string;

  @Prop()
  nextStep: string;

  @Prop({ type: Types.ObjectId, ref: 'Task' })
  task: Types.ObjectId;
}
export const StepSchema = SchemaFactory.createForClass(Step);

export type TaskDocument = Task & Document;
@Schema()
export class Task {
  @Prop()
  _id: Types.ObjectId;

  @Prop()
  taskId: string;

  @Prop()
  categorie: string;

  @Prop()
  cocxbq: string;

  @Prop()
  name: string;

  @Prop({ type: [{ type: Types.ObjectId, ref: 'Step' }] })
  steps: Types.ObjectId[];
}
export const TaskSchema = SchemaFactory.createForClass(Task);

export type SupportManagementDocument = SupportManagement & Document;
@Schema()
export class SupportManagement {
  @Prop()
  _id: Types.ObjectId;

  @Prop()
  agencyId: string;

  @Prop()
  professionnalRoleId: number;

  @Prop({
    type: [{ type: Types.ObjectId, ref: 'CustomerDemand' }],
  })
  'demandes'?: CustomerDemand[];

  // Méthode pour générer aléatoirement une demande client

  // Générer aléatoirement un objet SupportManagement
  static generateRandom(): SupportManagement {
    const randomId = new Types.ObjectId(); // Générez un nouvel ObjectId unique

    // Propriétés de SupportManagement à générer aléatoirement
    return {
      _id: randomId,
      agencyId: faker.person.firstName(),
      professionnalRoleId: faker.number.int({ min: 1, max: 10 }),
    };
  }
}
export const SupportManagementSchema =
  SchemaFactory.createForClass(SupportManagement);

export type CustomerDemandDocument = CustomerDemand & Document;
@Schema({ timestamps: true, versionKey: false })
export class CustomerDemand {
  @Prop()
  bankId: string;

  @Prop()
  demandId: number;

  @Prop()
  demandLabel: string;

  @Prop()
  demandType: string;

  @Prop()
  demandTypeLabel: string;

  @Prop()
  urgentIndicator: boolean;

  @Prop()
  regulatoryIndicator: boolean;

  @Prop({ type: [{ type: Types.ObjectId, ref: 'Step' }] })
  steps: Types.ObjectId[];

  @Prop({ type: Types.ObjectId, ref: 'SupportManagement' })
  support?: Types.ObjectId;

  static generateRandom(support: SupportManagement): CustomerDemand {
    const stepsCount = faker.number.int({ min: 1, max: 3 });
    const steps = Array.from(
      { length: stepsCount },
      () => new Types.ObjectId(),
    );
    return {
      bankId: faker.string.numeric(),
      demandId: faker.number.int(),
      demandLabel: faker.commerce.productName(),
      demandType: faker.animal.type(),
      urgentIndicator: Math.random() < 0.5,
      regulatoryIndicator: Math.random() < 0.5,
      demandTypeLabel: faker.person.firstName(),
      support: support._id,
      steps: steps,
    };
  }
}
export const CustomerDemandSchema =
  SchemaFactory.createForClass(CustomerDemand);
