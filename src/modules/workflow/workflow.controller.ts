import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Query,
  Req,
} from '@nestjs/common';
import { WorkflowService } from './workflow.service';
import { InitializationDto, FilterDto } from './dto/workflow.dto';

@Controller('workflow')
export class WorkflowController {
  constructor(private readonly workflowService: WorkflowService) {}

  @Post('/initialization')
  @HttpCode(201)
  async initialization(@Body() initDto: InitializationDto) {
    await this.workflowService.init(initDto.batchSize, initDto.totalDocuments);
  }

  @Get('/')
  @HttpCode(200)
  async list(@Query() filter: FilterDto) {
    const skip = (filter.page - 1) * filter.limit;

    const startTime = Date.now(); // Enregistrer le temps de début
    const data = await this.workflowService.findAll(filter, skip, filter.limit);

    return {
      metadata: {
        counter: data.length,
        executionTime: {
          unit: 'SECONDES',
          value: (Date.now() - startTime) / 1000,
        },
      },
      data,
    };
  }

  @Get('/count')
  @HttpCode(200)
  async count(@Query() filter: FilterDto) {
    const skip = (filter.page - 1) * filter.limit;

    const startTime = Date.now(); // Enregistrer le temps de début
    const data = await this.workflowService.findAll(filter, skip, filter.limit);
    const time = (Date.now() - startTime) / 1000;

    return {
      counter: data.length,
      executionTime: { unit: 'SECONDES', value: time },
    };
  }

  @Delete('/delete')
  @HttpCode(HttpStatus.OK)
  async deleteAllData() {
    return this.workflowService.deleteAllData();
  }
}
